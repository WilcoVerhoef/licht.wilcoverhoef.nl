package main

import (
	// "context"
	// "crypto/tls"
	"fmt"

	// "github.com/aws/aws-lambda-go/events"
	// "github.com/aws/aws-lambda-go/lambda"
	// "github.com/aws/aws-lambda-go/lambdacontext"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

var opts =
    mqtt.NewClientOptions().
    AddBroker("mqtt://mqtt.twinkly.com:1883").
    // AddBroker("localhost:8883").
    SetClientID("98CDAC40700D").
    SetUsername("twinkly_noauth").
    SetPassword("98:cd:ac:40:70:0d").
    SetDefaultPublishHandler(messagePubHandler).
    SetOnConnectHandler(connectHandler).
    SetConnectionLostHandler(connectLostHandler)
    // SetTLSConfig(&tls.Config{})

var client = mqtt.NewClient(opts)

var messagePubHandler mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
    fmt.Printf("Received message: %s from topic: %s\n", msg.Payload(), msg.Topic())
}

var connectHandler mqtt.OnConnectHandler = func(client mqtt.Client) {
    fmt.Println("Connected")
}

var connectLostHandler mqtt.ConnectionLostHandler = func(client mqtt.Client, err error) {
    fmt.Printf("Connect lost: %v", err)
}

// func handler(ctx context.Context, request events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
    
    
//     lc, ok := lambdacontext.FromContext(ctx)
//     if !ok {
//         return &events.APIGatewayProxyResponse{
//             StatusCode: 503,
//             Body:       "Something went wrong :(",
//         }, nil
//     }

//     cc := lc.ClientContext

//     return &events.APIGatewayProxyResponse{
//         StatusCode: 200,
//         Body:       "Hello, " + cc.Client.AppTitle,
//     }, nil
// }

func main() {
    token := client.Connect()

    token.WaitTimeout(6e9)

    fmt.Println(token.Error())
    fmt.Println(client.IsConnected())

    var name string;
    fmt.Scanf("%s", &name);
    fmt.Println("Hello, " + name);
}
