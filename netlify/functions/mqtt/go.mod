module mqtt

go 1.16

require (
	github.com/aws/aws-lambda-go v1.27.1
	github.com/eclipse/paho.mqtt.golang v1.3.5
)
